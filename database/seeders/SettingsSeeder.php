<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;


class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['key' => 'currency_rate_dkk', 'value' => '6.45'],
        ];

        foreach ($items as $item) {
            Setting::create($item);
        }
    }
}
