<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {

    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');

    /* For unauthorized users */
    Route::post('login', 'UserController@authenticate')->name('users.authenticate');
    Route::post('register', 'UserController@register')->name('users.register');
    Route::get('forgot-password/{email}', 'UserController@forgotPassword')->name('forgotPassword');
    Route::post('check-token', 'UserController@checkToken')->name('checkToken');
    Route::post('reset/process', 'UserController@setNewPassword')->name('setNewPassword');

    /* For authorized users */
    Route::group(['middleware' => ['auth:api', 'verified']], function () {

        /* Settings */
        Route::apiResource('settings', 'SettingController')->except('update', 'destroy');

        /* Users */
        Route::get('users/current-user', 'UserController@currentUser')->name('users.currentUser');
        Route::post('users/change-avatar/{user}', 'UserController@changeAvatar')->name('users.changeAvatar');
        Route::post('users/change-password/{user}', 'UserController@changePassword')->name('users.changePassword');
        Route::apiResource('users', 'UserController')->except('store', 'destroy');

        /* Roles */
        Route::apiResource('roles', 'RoleController')->except('show', 'store', 'update', 'destroy');

        /* Nfts */
        Route::get('nfts/nfts-for-sale', 'NftController@getNftsForSale')->name('nfts.getNftsForSale');
        Route::get('nfts/all-nfts-of-all-users', 'NftController@allNftsOfAllUsers')->name('nfts.allNftsOfAllUsers');
        Route::get('nfts/by-user-id/{user_id}', 'NftController@allNftsByUserId')->name('nfts.allNftsByUserId');
        Route::get('nfts/send-as-gift/{nft}/{address}', 'NftController@sendNftAsGift')->name('nfts.sendNftAsGift');
        Route::post('nfts/synchronization-nfts', 'NftController@synchronizationNfts')->name('nfts.synchronizationNfts');
        Route::apiResource('nfts', 'NftController')->except('update', 'destroy');
    });
});
