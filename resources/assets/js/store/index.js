import general from './general'
import nfts from './nfts'
import roles from './roles'
import settings from './settings'
import users from './users'

export default {
    modules: {
        general,
        nfts,
        roles,
        settings,
        users
    }
}
