<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\NftRequest;
use App\Services\{NftService};
use App\Models\{Nft};
use Auth;

class NftController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:author')->only('store');
    }

    public function index() {
        return response()->json(Nft::where(['user_id' => Auth::id()])->get(), 200);
    }

    public function allNftsOfAllUsers() {
        return response()->json(Nft::with('user')->get(), 200);
    }

    public function allNftsByUserId($user_id) {
        return response()->json(Nft::where(['user_id' == $user_id])->get(), 200);
    }

    public function getNftsForSale() {
        return response()->json(Nft::where(['price' != null])->has('drops', 0)->get(), 200);
    }

    public function store(NftRequest $request, NftService $nftService) {
        if ($request->copies) return $nftService->createCopiesNft($request);
        return $nftService->createNft($request);
    }

    public function show(Nft $nft) {
        return response()->json($nft->load('user'), 200);
    }
}
