"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_ResetPassword_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      form: {
        new_password: '',
        confirmPass: ''
      },
      verifyToken: false,
      // For verify hash.
      isPasswordConfirm: false // Label that checks password confirm

    };
  },
  created: function created() {
    this.verifyHash();
  },
  methods: {
    verifyHash: function verifyHash() {
      var _this = this;

      this.$store.dispatch('users/verifyHash', this.$route.params.hash).then(function (res) {
        if (true === res.success) {
          _this.verifyToken = true; // Set verify status - true
        }
      });
    },
    changePasswordWhenForgot: function changePasswordWhenForgot(e) {
      var _this2 = this;

      e.preventDefault();

      if (this.confirmPassword()) {
        this.$store.dispatch('users/changePasswordWhenForgot', {
          token: this.$route.params.hash,
          password: this.form.new_password
        }).then(function (res) {
          _this2.$router.push('/');
        });
      }
    },
    confirmPassword: function confirmPassword() {
      if (this.form.new_password === this.form.confirmPass) {
        this.isPasswordConfirm = true;
      } else {
        this.isPasswordConfirm = false;
      }

      return this.isPasswordConfirm;
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/ResetPassword.vue":
/*!*****************************************************!*\
  !*** ./resources/assets/js/pages/ResetPassword.vue ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=template&id=935215c6& */ "./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6&");
/* harmony import */ var _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__.render,
  _ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/ResetPassword.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ResetPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6&":
/*!************************************************************************************!*\
  !*** ./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6& ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_935215c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ResetPassword.vue?vue&type=template&id=935215c6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/ResetPassword.vue?vue&type=template&id=935215c6& ***!
  \***************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "enter-container" }, [
    _c("div", { staticClass: "enter-main", staticStyle: { width: "52%" } }, [
      _c("div", { staticClass: "enter-block" }, [
        _vm.verifyToken
          ? _c("div", { staticClass: "enter-block__body" }, [
              _c(
                "h3",
                {
                  staticClass: "enter-title",
                  staticStyle: { "margin-top": "3rem" },
                },
                [_vm._v("Reset password")]
              ),
              _vm._v(" "),
              _c("form", { on: { submit: _vm.changePasswordWhenForgot } }, [
                _c("div", [
                  _c("label", { attrs: { for: "password" } }, [
                    _vm._v("New password"),
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.new_password,
                        expression: "form.new_password",
                      },
                    ],
                    staticClass: "form-control input-black",
                    attrs: {
                      type: "password",
                      name: "password",
                      id: "password",
                      required: "",
                    },
                    domProps: { value: _vm.form.new_password },
                    on: {
                      input: function ($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "new_password", $event.target.value)
                      },
                    },
                  }),
                ]),
                _vm._v(" "),
                _c("div", { staticStyle: { "margin-top": "1rem" } }, [
                  _c("label", { attrs: { for: "password" } }, [
                    _vm._v("Confirm password"),
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.confirmPass,
                        expression: "form.confirmPass",
                      },
                    ],
                    staticClass: "form-control input-black",
                    attrs: {
                      type: "password",
                      name: "confirmPass",
                      id: "confirmPass",
                      required: "",
                    },
                    domProps: { value: _vm.form.confirmPass },
                    on: {
                      input: function ($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "confirmPass", $event.target.value)
                      },
                    },
                  }),
                ]),
                _vm._v(" "),
                _c("hr"),
                _vm._v(" "),
                _c("div", { staticClass: "flex flex-center" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      staticStyle: {
                        "border-radius": "20px",
                        "font-size": "20px",
                      },
                      attrs: {
                        type: "submit",
                        disabled:
                          _vm.form.new_password == "" ||
                          _vm.form.confirmPass == "" ||
                          _vm.form.new_password != _vm.form.confirmPass,
                      },
                    },
                    [
                      _vm._v(
                        "\n                            Save password\n                        "
                      ),
                    ]
                  ),
                ]),
              ]),
            ])
          : _vm._e(),
      ]),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
    _vm._v(" "),
    _c("img", {
      staticClass: "how-toBg",
      attrs: { src: "assets/img/how-to-action_BG.png", alt: "bg-img" },
    }),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);