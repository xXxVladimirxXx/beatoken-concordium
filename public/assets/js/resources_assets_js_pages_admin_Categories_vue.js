"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_admin_Categories_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'Categories',
  data: function data() {
    return {
      categories: [],
      sets: [],
      releases: [],
      packs: [],
      set: {
        name: ''
      },
      release: {
        name: '',
        set_id: 0
      },
      pack: {
        name: '',
        release_id: 0,
        price: 0.01,
        file: {}
      }
    };
  },
  created: function created() {
    this.getCategories();
  },
  methods: {
    processFileCreate: function processFileCreate(e) {
      this.pack.file = e.target.files[0];
    },
    addSet: function addSet() {
      var _this = this;

      if (!this.set.name) return false;
      this.$store.dispatch('categories/addSet', this.set).then(function () {
        _this.set = {
          name: ''
        };

        _this.getCategories();
      });
    },
    addRelease: function addRelease() {
      var _this2 = this;

      if (!this.release.name || !this.release.set_id) {
        this.release = {
          name: '',
          set_id: 0
        };
        return false;
      }

      this.$store.dispatch('categories/addRelease', this.release).then(function () {
        _this2.release = {
          name: '',
          set_id: 0
        };

        _this2.getCategories();
      });
    },
    addPack: function addPack() {
      var _this3 = this;

      if (!this.pack.name || !this.pack.release_id || !this.pack.file) {
        this.pack = {
          name: '',
          release_id: 0,
          price: null,
          file: {}
        };
        return false;
      }

      var formData = new FormData();
      if (this.pack.name) formData.append('name', this.pack.name);
      if (this.pack.release_id) formData.append('release_id', this.pack.release_id);
      if (this.pack.price) formData.append('price', this.pack.price);
      if (this.pack.file) formData.append('file', this.pack.file);
      this.$store.dispatch('categories/addPack', formData).then(function () {
        _this3.pack = {
          name: '',
          release_id: 0,
          price: null,
          file: {}
        };

        _this3.getCategories();
      });
    },
    getCategories: function getCategories() {
      var _this4 = this;

      this.$store.dispatch('categories/getCategories').then(function (res) {
        _this4.categories = res.categories;
        _this4.sets = res.sets;
        _this4.releases = res.releases;
        _this4.packs = res.packs;
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/admin/Categories.vue":
/*!********************************************************!*\
  !*** ./resources/assets/js/pages/admin/Categories.vue ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Categories.vue?vue&type=template&id=542a0919& */ "./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919&");
/* harmony import */ var _Categories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Categories.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Categories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__.render,
  _Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/admin/Categories.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Categories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Categories.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Categories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919&":
/*!***************************************************************************************!*\
  !*** ./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Categories_vue_vue_type_template_id_542a0919___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Categories.vue?vue&type=template&id=542a0919& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Categories.vue?vue&type=template&id=542a0919& ***!
  \******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h3", { staticClass: "admin-table__title" }, [_vm._v("Categories")]),
    _vm._v(" "),
    _c("div", { staticClass: "categories-wrapper" }, [
      _c("div", { staticClass: "accordion-block" }, [
        _c("div", { staticClass: "accordion", attrs: { id: "accordion" } }, [
          _c("div", { staticClass: "accordion-item" }, [
            _vm._m(0),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "accordion-collapse collapse show",
                attrs: {
                  id: "collapseOne",
                  "aria-labelledby": "headingOne",
                  "data-bs-parent": "#accordion",
                },
              },
              [
                _c("div", { staticClass: "accordion-body" }, [
                  _c("div", { staticClass: "mb-3" }, [
                    _c(
                      "label",
                      {
                        staticClass: "form-label",
                        attrs: { for: "formControlInput1" },
                      },
                      [_vm._v("Set name *")]
                    ),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.set.name,
                          expression: "set.name",
                        },
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "formControlInput1",
                        placeholder: "Enter set name",
                      },
                      domProps: { value: _vm.set.name },
                      on: {
                        input: function ($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.set, "name", $event.target.value)
                        },
                      },
                    }),
                  ]),
                  _vm._v(" "),
                  _c(
                    "button",
                    { staticClass: "submit", on: { click: _vm.addSet } },
                    [_vm._v("Add set")]
                  ),
                ]),
              ]
            ),
          ]),
          _vm._v(" "),
          _vm.sets.length
            ? _c("div", { staticClass: "accordion-item" }, [
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "accordion-collapse collapse",
                    attrs: {
                      id: "collapseTwo",
                      "aria-labelledby": "headingTwo",
                      "data-bs-parent": "#accordion",
                    },
                  },
                  [
                    _c("div", { staticClass: "accordion-body" }, [
                      _c("div", { staticClass: "mb-3" }, [
                        _c(
                          "label",
                          {
                            staticClass: "form-label",
                            attrs: { for: "formControlInput2" },
                          },
                          [_vm._v("Release name *")]
                        ),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.release.name,
                              expression: "release.name",
                            },
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            id: "formControlInput2",
                            placeholder: "Enter release name",
                          },
                          domProps: { value: _vm.release.name },
                          on: {
                            input: function ($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.release, "name", $event.target.value)
                            },
                          },
                        }),
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-3 select-set__block" }, [
                        _c("label", { staticClass: "form-label" }, [
                          _vm._v("Parent set *"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.release.set_id,
                                expression: "release.set_id",
                              },
                            ],
                            staticClass: "select",
                            on: {
                              change: function ($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function (o) {
                                    return o.selected
                                  })
                                  .map(function (o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.release,
                                  "set_id",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              },
                            },
                          },
                          [
                            _c(
                              "option",
                              { attrs: { selected: "", value: "0" } },
                              [_vm._v("Select set")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.sets, function (set, i) {
                              return _c(
                                "option",
                                { domProps: { value: set.id } },
                                [_vm._v(_vm._s(set.name))]
                              )
                            }),
                          ],
                          2
                        ),
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "submit",
                          on: { click: _vm.addRelease },
                        },
                        [_vm._v("Add release")]
                      ),
                    ]),
                  ]
                ),
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.sets.length && _vm.releases.length
            ? _c("div", { staticClass: "accordion-item" }, [
                _vm._m(2),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "accordion-collapse collapse",
                    attrs: {
                      id: "collapseThree",
                      "aria-labelledby": "headingThree",
                      "data-bs-parent": "#accordion",
                    },
                  },
                  [
                    _c("div", { staticClass: "accordion-body" }, [
                      _c("div", { staticClass: "mb-3 select-set__block" }, [
                        _c(
                          "label",
                          {
                            staticClass: "form-label",
                            attrs: { for: "formControlInput3" },
                          },
                          [_vm._v("Pack name *")]
                        ),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.pack.name,
                              expression: "pack.name",
                            },
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            id: "formControlInput3",
                            placeholder: "Enter pack name",
                          },
                          domProps: { value: _vm.pack.name },
                          on: {
                            input: function ($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.pack, "name", $event.target.value)
                            },
                          },
                        }),
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "flex-center" }, [
                        _c("div", [
                          _c(
                            "label",
                            {
                              staticClass: "label-photo btn btn-success",
                              attrs: { for: "nft_img" },
                            },
                            [_vm._v("Select file for pack *")]
                          ),
                          _vm._v(" "),
                          _c("label", [_vm._v(_vm._s(_vm.pack.file.name))]),
                          _vm._v(" "),
                          _c("input", {
                            staticStyle: { display: "none" },
                            attrs: {
                              type: "file",
                              id: "nft_img",
                              accept: "image/jpeg,image/png",
                            },
                            on: {
                              change: function ($event) {
                                return _vm.processFileCreate($event)
                              },
                            },
                          }),
                        ]),
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-3 select-set__block" }, [
                        _c(
                          "label",
                          {
                            staticClass: "form-label",
                            attrs: { for: "formControlInput4" },
                          },
                          [_vm._v("Pack price")]
                        ),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.pack.price,
                              expression: "pack.price",
                            },
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "number",
                            id: "formControlInput4",
                            placeholder: "Enter pack price",
                            step: "0.01",
                            min: "0.01",
                            max: "9999.99",
                          },
                          domProps: { value: _vm.pack.price },
                          on: {
                            input: function ($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.pack, "price", $event.target.value)
                            },
                          },
                        }),
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-3 select-set__block" }, [
                        _c("label", { staticClass: "form-label" }, [
                          _vm._v("Parent release *"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.pack.release_id,
                                expression: "pack.release_id",
                              },
                            ],
                            staticClass: "select",
                            on: {
                              change: function ($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function (o) {
                                    return o.selected
                                  })
                                  .map(function (o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.pack,
                                  "release_id",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              },
                            },
                          },
                          [
                            _c(
                              "option",
                              { attrs: { selected: "", value: "0" } },
                              [_vm._v("Select release")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.releases, function (release, i) {
                              return _c(
                                "option",
                                { domProps: { value: release.id } },
                                [
                                  _vm._v(
                                    _vm._s(
                                      "(" +
                                        release.set.name +
                                        ") - " +
                                        release.name
                                    )
                                  ),
                                ]
                              )
                            }),
                          ],
                          2
                        ),
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        { staticClass: "submit", on: { click: _vm.addPack } },
                        [_vm._v("Add pack")]
                      ),
                    ]),
                  ]
                ),
              ])
            : _vm._e(),
        ]),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "categories-table__block" }, [
        _c("table", { staticClass: "table table-bordered" }, [
          _vm._m(3),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.categories, function (category, i) {
              return _vm.categories.length
                ? _c("tr", [
                    _c("td", [_c("p", [_vm._v(_vm._s(category.name))])]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v(
                        _vm._s(
                          category.description
                            ? category.description
                            : "No description"
                        )
                      ),
                    ]),
                  ])
                : _vm._e()
            }),
            0
          ),
        ]),
      ]),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "h2",
      { staticClass: "accordion-header", attrs: { id: "headingOne" } },
      [
        _c(
          "button",
          {
            staticClass: "accordion-button",
            attrs: {
              type: "button",
              "data-bs-toggle": "collapse",
              "data-bs-target": "#collapseOne",
              "aria-expanded": "true",
              "aria-controls": "collapseOne",
            },
          },
          [
            _vm._v(
              "\n                            Create set\n                        "
            ),
          ]
        ),
      ]
    )
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "h2",
      { staticClass: "accordion-header", attrs: { id: "headingTwo" } },
      [
        _c(
          "button",
          {
            staticClass: "accordion-button collapsed",
            attrs: {
              type: "button",
              "data-bs-toggle": "collapse",
              "data-bs-target": "#collapseTwo",
              "aria-expanded": "false",
              "aria-controls": "collapseTwo",
            },
          },
          [
            _vm._v(
              "\n                            Create release\n                        "
            ),
          ]
        ),
      ]
    )
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "h2",
      { staticClass: "accordion-header", attrs: { id: "headingThree" } },
      [
        _c(
          "button",
          {
            staticClass: "accordion-button collapsed",
            attrs: {
              type: "button",
              "data-bs-toggle": "collapse",
              "data-bs-target": "#collapseThree",
              "aria-expanded": "false",
              "aria-controls": "collapseThree",
            },
          },
          [
            _vm._v(
              "\n                            Create pack\n                        "
            ),
          ]
        ),
      ]
    )
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Description")]),
      ]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);