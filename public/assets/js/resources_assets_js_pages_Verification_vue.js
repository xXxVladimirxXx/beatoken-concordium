"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_Verification_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'verification',
  data: function data() {
    return {};
  },
  created: function created() {
    var _this = this;

    return this.$store.dispatch('users/getCurrentUser').then(function (user) {
      if (user.email_verified_at) {
        _this.$router.push('/');
      }
    });
  },
  computed: {
    user: function user() {
      return this.$store.getters['users/getCurrentUser'];
    }
  },
  methods: {
    resend: function resend() {
      var _this2 = this;

      this.$loader(true);
      this.$store.dispatch('users/resend').then(function (resp) {
        _this2.$loader(false);

        _this2.$notify({
          title: 'Email verification link sent on your email',
          type: 'success'
        });
      })["catch"](function () {
        return _this2.$loader(false);
      });
    },
    logout: function logout() {
      this.$store.dispatch('users/logout');
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/Verification.vue":
/*!****************************************************!*\
  !*** ./resources/assets/js/pages/Verification.vue ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Verification.vue?vue&type=template&id=68eced58& */ "./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58&");
/* harmony import */ var _Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Verification.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__.render,
  _Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/Verification.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Verification.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58&":
/*!***********************************************************************************!*\
  !*** ./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58& ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Verification_vue_vue_type_template_id_68eced58___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Verification.vue?vue&type=template&id=68eced58& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Verification.vue?vue&type=template&id=68eced58& ***!
  \**************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "enter-container" }, [
    _c("div", { staticClass: "enter-main" }, [
      _c(
        "div",
        { staticClass: "enter-block", staticStyle: { width: "auto" } },
        [
          _c(
            "div",
            { staticClass: "enter-block__body" },
            [
              _c(
                "h3",
                {
                  staticClass: "enter-title",
                  staticStyle: { "margin-top": "3rem", "text-align": "center" },
                },
                [_vm._v("Verification")]
              ),
              _vm._v(" "),
              _c(
                "p",
                { staticStyle: { "text-align": "center", color: "#4f3f71" } },
                [
                  _vm._v(
                    "\n                    Email verification is needed for "
                  ),
                  _c("br"),
                  _vm._v("additional security measures"),
                  _c("br"),
                  _c("br"),
                  _vm._v("\n                    Check your email"),
                  _vm.user.email
                    ? _c("span", [_vm._v(" (" + _vm._s(_vm.user.email) + ")")])
                    : _vm._e(),
                  _vm._v(", "),
                  _c("br"),
                  _vm._v(
                    " we have sent you an activation link\n                "
                  ),
                ]
              ),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm.user.email && _vm.user != {}
                ? [
                    _c(
                      "p",
                      {
                        staticClass: "terms-privacy",
                        staticStyle: { "text-align": "center" },
                      },
                      [
                        _vm._v("Didn't receive a letter? "),
                        _c(
                          "a",
                          {
                            staticStyle: { cursor: "pointer" },
                            on: {
                              click: function ($event) {
                                return _vm.resend()
                              },
                            },
                          },
                          [_vm._v("Send again")]
                        ),
                      ]
                    ),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticClass: "terms-privacy",
                        staticStyle: { "text-align": "center" },
                      },
                      [
                        _vm._v("Not your mail? "),
                        _c(
                          "a",
                          {
                            staticStyle: { cursor: "pointer" },
                            on: {
                              click: function ($event) {
                                return _vm.logout()
                              },
                            },
                          },
                          [_vm._v("Logout")]
                        ),
                      ]
                    ),
                  ]
                : _vm._e(),
            ],
            2
          ),
        ]
      ),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
    _vm._v(" "),
    _c("img", {
      staticClass: "how-toBg",
      attrs: { src: "assets/img/how-to-action_BG.png", alt: "bg-img" },
    }),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);