"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_Signup_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      user: {
        email: '',
        name: '',
        password: ''
      }
    };
  },
  methods: {
    register: function register(event) {
      var _this = this;

      this.$loader(true);
      event.preventDefault();
      this.$store.dispatch('users/register', this.user).then(function () {
        _this.$store.dispatch('users/login', _this.user).then(function () {
          _this.$loader(false);

          _this.$router.push('verification');
        });
      })["catch"](function (e) {
        if (400 == e.status) {
          var title = 'The email you are trying to register has already been created';
        } else {
          var title = 'Server error. Please try again later';
        }

        _this.$loader(false);

        _this.$notify({
          title: title,
          type: 'error'
        });
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/Signup.vue":
/*!**********************************************!*\
  !*** ./resources/assets/js/pages/Signup.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Signup.vue?vue&type=template&id=639c8195& */ "./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195&");
/* harmony import */ var _Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Signup.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__.render,
  _Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/Signup.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Signup.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195&":
/*!*****************************************************************************!*\
  !*** ./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Signup_vue_vue_type_template_id_639c8195___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Signup.vue?vue&type=template&id=639c8195& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Signup.vue?vue&type=template&id=639c8195& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "enter-container" }, [
    _c("div", { staticClass: "enter-main" }, [
      _c("div", { staticClass: "enter-block" }, [
        _c(
          "div",
          { staticClass: "enter-block__body" },
          [
            _c(
              "h3",
              {
                staticClass: "enter-title",
                staticStyle: { "margin-top": "3rem" },
              },
              [_vm._v("Create account")]
            ),
            _vm._v(" "),
            _c("form", { on: { submit: _vm.register } }, [
              _c("div", [
                _c("label", { attrs: { for: "email" } }, [_vm._v("Email")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.email,
                      expression: "user.email",
                    },
                  ],
                  staticClass: "form-control input-black",
                  attrs: {
                    type: "email",
                    name: "email",
                    id: "email",
                    required: "",
                  },
                  domProps: { value: _vm.user.email },
                  on: {
                    input: function ($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "email", $event.target.value)
                    },
                  },
                }),
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("div", [
                _c("label", { attrs: { for: "email" } }, [
                  _vm._v("Display name"),
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.name,
                      expression: "user.name",
                    },
                  ],
                  staticClass: "form-control input-black",
                  attrs: {
                    type: "text",
                    name: "name",
                    id: "name",
                    maxlength: "21",
                    required: "",
                  },
                  domProps: { value: _vm.user.name },
                  on: {
                    input: function ($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "name", $event.target.value)
                    },
                  },
                }),
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("div", [
                _c("label", { attrs: { for: "password" } }, [
                  _vm._v("Password"),
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.password,
                      expression: "user.password",
                    },
                  ],
                  staticClass: "form-control input-black",
                  attrs: {
                    type: "password",
                    name: "password",
                    id: "password",
                    required: "",
                  },
                  domProps: { value: _vm.user.password },
                  on: {
                    input: function ($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "password", $event.target.value)
                    },
                  },
                }),
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._m(1),
            ]),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "already-have",
                staticStyle: { "margin-top": "40px" },
                attrs: { to: "/login" },
              },
              [_vm._v("Already have an account? Sign in")]
            ),
            _vm._v(" "),
            _vm._m(2),
          ],
          1
        ),
      ]),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
    _vm._v(" "),
    _c("img", {
      staticClass: "how-toBg",
      attrs: { src: "assets/img/how-to-action_BG.png", alt: "bg-img" },
    }),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "flex-center" }, [
      _c("input", {
        staticClass: "input-black",
        attrs: {
          type: "checkbox",
          name: "accept_terms",
          id: "accept_terms",
          required: "",
        },
      }),
      _vm._v(" "),
      _c(
        "label",
        {
          staticStyle: { "margin-left": "5px", "font-size": "85%" },
          attrs: { for: "accept_terms" },
        },
        [
          _vm._v("I have read and agree to the "),
          _c(
            "a",
            {
              attrs: {
                href: "https://beatoken.com/terms-condition",
                target: "_blank",
              },
            },
            [_vm._v("terms & conditions")]
          ),
        ]
      ),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "flex flex-center" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-primary",
          staticStyle: { "border-radius": "20px", "font-size": "20px" },
        },
        [_vm._v("Register")]
      ),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "terms-privacy" }, [
      _vm._v(
        "\n                    By signing up, you acknowledge that you have read and\n                    agree all applicable "
      ),
      _c("a", { attrs: { href: "https://beatoken.com/terms-condition/" } }, [
        _vm._v("Terms of Service"),
      ]),
      _vm._v(" and our\n                    "),
      _c("a", { attrs: { href: "https://beatoken.com/terms-condition/" } }, [
        _vm._v("Privacy Policy"),
      ]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);