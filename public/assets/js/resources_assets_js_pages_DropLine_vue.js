"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_DropLine_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'drop-line',
  data: function data() {
    return {
      drop: {},
      dropLine: {},
      dropLinesCount: 0,
      timeToStartBuy: 0,
      displayTime: '',
      timer: null
    };
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    this.$loader(true);
    this.$store.dispatch('drop_lines/myTimeToStartBuyDrop', this.$route.params.drop_id).then(function (resp) {
      if (resp.redirectToBuy) _this.$router.push('/drop-buy/' + _this.$route.params.drop_id);
      if (resp.redirectToDrop) _this.$router.push('/drop/' + _this.$route.params.drop_id);
      _this.dropLine = resp.dropLine;
      _this.timeToStartBuy = resp.timeToStartBuy;
      _this.dropLinesCount = resp.dropLinesCount;

      _this.startTimer();

      setTimeout(function () {
        _this.$loader(false);
      }, 1000);
    });
  },
  created: function created() {
    this.getDrop();
  },
  watch: {
    timeToStartBuy: function timeToStartBuy(time) {
      if (time === 0) {
        this.stopTimer();
      }
    }
  },
  methods: {
    startTimer: function startTimer() {
      var _this2 = this;

      this.timer = setInterval(function () {
        _this2.timeToStartBuy--;
        var minutes = parseInt(_this2.timeToStartBuy / 60, 10);
        var seconds = parseInt(_this2.timeToStartBuy % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        _this2.displayTime = "".concat(minutes, "min  ").concat(seconds, "sec");
      }, 1000);
    },
    stopTimer: function stopTimer() {
      clearTimeout(this.timer);
      this.$router.push('/drop-buy/' + this.drop.id);
    },
    getDrop: function getDrop() {
      var _this3 = this;

      this.$store.dispatch('drops/showDrop', this.$route.params.drop_id).then(function (drop) {
        _this3.drop = drop;
        if (!drop.nfts.length) _this3.$router.push('/drop/' + _this3.$route.params.drop_id);
      })["catch"](function () {
        return _this3.$router.push('/drops');
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/DropLine.vue":
/*!************************************************!*\
  !*** ./resources/assets/js/pages/DropLine.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DropLine.vue?vue&type=template&id=4e797d80& */ "./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80&");
/* harmony import */ var _DropLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DropLine.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DropLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__.render,
  _DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/DropLine.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DropLine.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropLine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80&":
/*!*******************************************************************************!*\
  !*** ./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropLine_vue_vue_type_template_id_4e797d80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DropLine.vue?vue&type=template&id=4e797d80& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropLine.vue?vue&type=template&id=4e797d80& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("main", { staticClass: "drop-full__screen-wrapper count" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "drop-count__container" }, [
          _vm.timeToStartBuy
            ? _c("div", { staticClass: "drop-count-wrapper" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "drop-count__title-block" }, [
                  _c("h3", [_vm._v("YOU ARE NOW IN LINE")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "pack-desc" }, [
                    _vm._v(
                      "\n                            You are in line for " +
                        _vm._s(_vm.drop.name) +
                        " (" +
                        _vm._s(_vm.drop.release_name) +
                        ")"
                    ),
                    _c("br"),
                    _vm._v(
                      " When\n                            it is your turn, you will have 20 mintunes to complete the\n                            order. When its your turn, it takes up to 1 min to re-direct you\n                            to complete the order.\n                        "
                    ),
                  ]),
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "drop-count__count-block" }, [
                  _c("p", { staticClass: "user-in-line" }, [
                    _vm._v(
                      "Number of users in line ahead of you: " +
                        _vm._s(_vm.dropLinesCount)
                    ),
                  ]),
                  _vm._v(" "),
                  _vm.displayTime
                    ? _c("h2", { staticStyle: { color: "white" } }, [
                        _vm._v(_vm._s(_vm.displayTime)),
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._m(1),
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "live-line__block" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "live-line__link",
                        staticStyle: { cursor: "pointer" },
                        attrs: { to: "/drops" },
                      },
                      [_vm._v("Leave the line")]
                    ),
                  ],
                  1
                ),
              ])
            : _vm._e(),
        ]),
      ]),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "drop-count__logo-block" }, [
      _c("img", {
        staticClass: "img-logo",
        attrs: { src: "assets/img/logo.png", alt: "logo" },
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "text-logo",
        attrs: { src: "assets/img/beatokenTextLogo.svg", alt: "text-logo" },
      }),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "status-update" }, [
      _vm._v("\n                            Status last update "),
      _c("span", { staticClass: "update-time" }),
      _vm._v(
        "\n                            Copenhagen time\n                        "
      ),
    ])
  },
]
render._withStripped = true



/***/ })

}]);