"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_Marketplace_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'Profile',
  data: function data() {
    return {
      nfts: [],
      loader: true,
      user: {},
      settings: {}
    };
  },
  created: function created() {
    this.getAllSettings();
    this.allNftsForSale();
  },
  filters: {
    formatPrice: function formatPrice(price) {
      if (price && '00' == price.split(".")[1]) return price.split(".")[0];
      return price;
    }
  },
  methods: {
    getAllSettings: function getAllSettings() {
      var _this = this;

      this.$store.dispatch('settings/allSettings').then(function (settings) {
        _this.settings = settings;
      });
    },
    allNftsForSale: function allNftsForSale() {
      var _this2 = this;

      this.loader = true;
      this.$store.dispatch('nfts/allNftsForSale').then(function (nfts) {
        _this2.nfts = nfts;
        _this2.loader = false;
      })["catch"](function (error) {
        _this2.loader = false;
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/Marketplace.vue":
/*!***************************************************!*\
  !*** ./resources/assets/js/pages/Marketplace.vue ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Marketplace.vue?vue&type=template&id=29badc9e& */ "./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e&");
/* harmony import */ var _Marketplace_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Marketplace.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Marketplace_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__.render,
  _Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/Marketplace.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Marketplace_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Marketplace.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Marketplace_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e&":
/*!**********************************************************************************!*\
  !*** ./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Marketplace_vue_vue_type_template_id_29badc9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Marketplace.vue?vue&type=template&id=29badc9e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Marketplace.vue?vue&type=template&id=29badc9e& ***!
  \*************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container" }, [
      _vm._m(0),
      _vm._v(" "),
      _vm._m(1),
      _vm._v(" "),
      _c("div", { staticClass: "music-card__container" }, [
        _vm.nfts.length
          ? _c(
              "ul",
              { staticClass: "music-card__list marketplace" },
              _vm._l(_vm.nfts, function (item, i) {
                return item.metadata
                  ? _c(
                      "li",
                      { key: i, staticClass: "music-card__item" },
                      [
                        _c(
                          "router-link",
                          { attrs: { to: "/nft/" + item.id } },
                          [
                            _c(
                              "div",
                              {
                                staticClass: "music-img",
                                staticStyle: { "text-align": "center" },
                              },
                              [
                                _c("img", {
                                  attrs: {
                                    src: item.metadata.cover_image,
                                    width: "197",
                                    onerror: "this.hidden=true",
                                  },
                                }),
                              ]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "music-desc__block" }, [
                              _c("div", { staticClass: "music-title" }, [
                                _c("p", { staticClass: "item-music__title" }, [
                                  _vm._v(_vm._s(item.metadata.name)),
                                ]),
                              ]),
                              _vm._v(" "),
                              item.type
                                ? _c(
                                    "p",
                                    { staticClass: "item-music__tariff-plan" },
                                    [
                                      _vm._v(
                                        "\n                              " +
                                          _vm._s(item.type.attribute_value) +
                                          "\n                              "
                                      ),
                                      item.numbering
                                        ? _c("span", [
                                            _vm._v(
                                              "#1 / " +
                                                _vm._s(
                                                  item.numbering.attribute_value
                                                )
                                            ),
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "plan-icon" }, [
                                        _vm._v(
                                          _vm._s(
                                            item.type.attribute_value.charAt(0)
                                          )
                                        ),
                                      ]),
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c("p", { staticClass: "sale-status" }, [
                                _vm._v("Price"),
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "item-music__price" }, [
                                _vm.settings.currency_rate_dkk
                                  ? _c("span", [
                                      _vm._v(
                                        _vm._s(
                                          Number(
                                            parseFloat(
                                              item.price *
                                                _vm.settings.currency_rate_dkk
                                            )
                                          ).toFixed(2)
                                        ) + "kr."
                                      ),
                                    ])
                                  : _vm._e(),
                              ]),
                            ]),
                          ]
                        ),
                      ],
                      1
                    )
                  : _vm._e()
              }),
              0
            )
          : _c("div", { staticStyle: { margin: "10rem 0", color: "#fff" } }, [
              _vm.nfts.length || _vm.loader
                ? _c("div", { staticClass: "flex-center" }, [
                    _c("img", {
                      attrs: { src: "assets/img/loader.gif", width: "100px" },
                    }),
                  ])
                : _c("p", [_vm._v("No NFT’s are up for sale at the moment")]),
            ]),
      ]),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "marketplace-title" }, [
      _c("h3", [_vm._v("Marketplace")]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "sort-container__profile" }, [
      _c(
        "div",
        { staticClass: "additional-sort additional-sort__marketplace" },
        [
          _c(
            "select",
            {
              staticClass: "form-select sort-btn",
              attrs: { "aria-label": "sort" },
            },
            [
              _c("option", { attrs: { selected: "" } }, [_vm._v("Artist")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "1" } }, [
                _vm._v("Recieved (oldest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "2" } }, [
                _vm._v("Recieved (newest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "3" } }, [
                _vm._v("Highest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "4" } }, [
                _vm._v("Lowest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "5" } }, [
                _vm._v("Lowest ask (assending)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "6" } }, [
                _vm._v("Lowest ask (desending)"),
              ]),
            ]
          ),
          _vm._v(" "),
          _c(
            "select",
            {
              staticClass: "form-select sort-btn",
              attrs: { "aria-label": "sort" },
            },
            [
              _c("option", { attrs: { selected: "" } }, [_vm._v("Genre")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "1" } }, [
                _vm._v("Recieved (oldest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "2" } }, [
                _vm._v("Recieved (newest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "3" } }, [
                _vm._v("Highest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "4" } }, [
                _vm._v("Lowest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "5" } }, [
                _vm._v("Lowest ask (assending)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "6" } }, [
                _vm._v("Lowest ask (desending)"),
              ]),
            ]
          ),
          _vm._v(" "),
          _c(
            "select",
            {
              staticClass: "form-select sort-btn",
              attrs: { "aria-label": "sort" },
            },
            [
              _c("option", { attrs: { selected: "" } }, [_vm._v("Series")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "1" } }, [
                _vm._v("Recieved (oldest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "2" } }, [
                _vm._v("Recieved (newest)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "3" } }, [
                _vm._v("Highest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "4" } }, [
                _vm._v("Lowest Serial Number"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "5" } }, [
                _vm._v("Lowest ask (assending)"),
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "6" } }, [
                _vm._v("Lowest ask (desending)"),
              ]),
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "range-block" }, [
            _c("p", { staticClass: "range-title" }, [_vm._v("Price range")]),
            _vm._v(" "),
            _c("div", { staticClass: "rangewrapper" }, [
              _c("div", { staticClass: "sliderfill" }, [
                _c("input", {
                  staticClass: "customrange",
                  attrs: {
                    type: "range",
                    min: "0",
                    max: "10000",
                    value: "2000",
                  },
                }),
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "sliderthumb" }),
              _vm._v(" "),
              _c("p", { staticClass: "slidervalue" }, [_vm._v("kr")]),
            ]),
          ]),
        ]
      ),
    ])
  },
]
render._withStripped = true



/***/ })

}]);