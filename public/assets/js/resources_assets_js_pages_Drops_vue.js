"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_Drops_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'drops',
  data: function data() {
    return {
      currentDrop: {},
      settings: {}
    };
  },
  created: function created() {
    this.getAllSettings();
  },
  methods: {
    getAllSettings: function getAllSettings() {
      var _this = this;

      this.$store.dispatch('settings/allSettings').then(function (settings) {
        _this.settings = settings;

        _this.getCurrentDrop();
      });
    },
    getCurrentDrop: function getCurrentDrop() {
      var _this2 = this;

      this.$store.dispatch('drops/getCurrentDrop').then(function (currentDrop) {
        var commissionForBuyDrop = currentDrop.price / 100 * (Number(_this2.settings.percent_drop_seller) + Number(_this2.settings.percent_drop_buyer)); // consider the commission

        var totalSum = commissionForBuyDrop + currentDrop.nfts[0].price * currentDrop.number_nfts;
        _this2.currentDrop = currentDrop;
        _this2.currentDrop.totalSum = totalSum;
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/Drops.vue":
/*!*********************************************!*\
  !*** ./resources/assets/js/pages/Drops.vue ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Drops.vue?vue&type=template&id=b6fdd512& */ "./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512&");
/* harmony import */ var _Drops_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Drops.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Drops_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__.render,
  _Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/Drops.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Drops_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Drops.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Drops_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512&":
/*!****************************************************************************!*\
  !*** ./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Drops_vue_vue_type_template_id_b6fdd512___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Drops.vue?vue&type=template&id=b6fdd512& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/Drops.vue?vue&type=template&id=b6fdd512& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "header-drop" }, [
      _c("div", { staticClass: "how-to-action drop" }, [
        _c("div", { staticClass: "how-toBg" }),
        _vm._v(" "),
        _c("h3", { staticClass: "visually-hidden" }, [
          _vm._v("Highlighted Drops"),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "container" }, [
          _c("h2", { staticClass: "section-drop-title" }, [
            _vm._v("Highlighted Drop"),
          ]),
          _vm._v(" "),
          _vm.currentDrop.nfts
            ? _c("div", { staticClass: "sets-block" }, [
                _c(
                  "div",
                  {
                    staticClass: "set",
                    class: {
                      "set-one": _vm.currentDrop.nfts.length,
                      "set-two": !_vm.currentDrop.nfts.length,
                    },
                  },
                  [
                    _c("div", { staticClass: "bg-txt" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "set-block" }, [
                      _c("div", { staticClass: "set-one__txt" }, [
                        _vm.currentDrop.nfts && _vm.currentDrop.nfts.length
                          ? _c("span", { staticClass: "set-state" }, [
                              _vm._v("Now available"),
                            ])
                          : _c("span", { staticClass: "set-state" }, [
                              _vm._v("Sold out"),
                            ]),
                        _vm._v(" "),
                        _c("h3", [
                          _vm._v(
                            "\n                                    " +
                              _vm._s(_vm.currentDrop.name) +
                              "\n                                    "
                          ),
                          _c("span", [_vm._v("(Drop 1)")]),
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "common" }, [
                          _vm._v("Common | Contains "),
                          _c("span", [
                            _vm._v(_vm._s(_vm.currentDrop.number_nfts)),
                          ]),
                          _vm._v(" cards"),
                        ]),
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "set-one__img" },
                        [
                          _c(
                            "router-link",
                            { attrs: { to: "/drop/" + _vm.currentDrop.id } },
                            [
                              _c("img", {
                                staticClass: "set-img",
                                attrs: {
                                  src: _vm.currentDrop.full_uri,
                                  width: "190",
                                  height: "224",
                                  alt: "set-img",
                                },
                              }),
                            ]
                          ),
                        ],
                        1
                      ),
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "price" }, [
                      _vm._v("Price:\n                            "),
                      _vm.settings.currency_rate_dkk
                        ? _c("span", [
                            _vm._v(
                              "\n                                " +
                                _vm._s(
                                  Number(
                                    parseFloat(
                                      _vm.currentDrop.totalSum *
                                        _vm.settings.currency_rate_dkk
                                    )
                                  ).toFixed(2)
                                ) +
                                "\n                            "
                            ),
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("span", [
                        _vm._v("kr "),
                        _c("small", [
                          _vm._v(
                            "(with fee " +
                              _vm._s(_vm.settings.percent_drop_buyer) +
                              "%)"
                          ),
                        ]),
                      ]),
                    ]),
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "set set-two" }, [
                  _c("div", { staticClass: "bg-txt" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "set-block" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "set-two__img" },
                      [
                        _c("router-link", { attrs: { to: "/drops" } }, [
                          _c("img", {
                            staticClass: "set-img",
                            attrs: {
                              src: "https://beatoken.com/wp-content/themes/beatoken/core/img/drop_holo_pack.png",
                              width: "110",
                              height: "121",
                              alt: "set-img",
                            },
                          }),
                        ]),
                      ],
                      1
                    ),
                  ]),
                  _vm._v(" "),
                  _vm._m(1),
                ]),
              ])
            : _vm._e(),
        ]),
      ]),
    ]),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "set-two__txt" }, [
      _c("span", { staticClass: "set-state" }, [_vm._v("Sold out")]),
      _vm._v(" "),
      _c("h3", [
        _vm._v(
          "\n                                    Holo set\n                                    "
        ),
        _c("span", [_vm._v("(Drop 2)")]),
      ]),
      _vm._v(" "),
      _c("p", [_vm._v("Common gold")]),
      _vm._v(" "),
      _c("p", { staticClass: "common" }, [
        _vm._v("Contains "),
        _c("span", [_vm._v("3")]),
        _vm._v(" cards"),
      ]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "price" }, [
      _vm._v("Price: "),
      _c("span", [_vm._v("25kr.")]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "section-open-packs" }, [
      _c("h3", { staticClass: "visually-hidden" }, [
        _vm._v("Open packs to find exciting new moments"),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("h2", { staticClass: "section-drop-title" }, [
          _vm._v(
            "\n                Open packs to find exciting new moments\n            "
          ),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "open-packs__container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "open-packs__block item col-lg-4" }, [
              _c("h3", [_vm._v("When do new drops happen?")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n                            During our Beta, drop times will vary, so make\n                            sure you watch for announcements.\n                        "
                ),
              ]),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "open-packs__block col-lg-4" }, [
              _c("h3", [_vm._v("Where are drop announcements?")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n                            Sign up via email and recieve our newsletter to never miss a\n                            drop\n                        "
                ),
              ]),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "open-packs__block col-lg-4" }, [
              _c("h3", [_vm._v("What about sold out packs?")]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n                            You can still find collectibles from the packs in the\n                            "
                ),
                _c(
                  "a",
                  { attrs: { href: "https://my.beatoken.com/marketplace" } },
                  [_vm._v("Marketplace")]
                ),
                _vm._v(" from other collectors\n                        "),
              ]),
            ]),
          ]),
        ]),
      ]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "section-packs" }, [
      _c("h3", { staticClass: "visually-hidden" }, [_vm._v("Packs")]),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "previous-packs" }, [
          _c("div", { staticClass: "packs-title" }, [
            _c("h3", { staticClass: "section-drop-title" }, [
              _vm._v("Previous packs"),
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "\n                        These packs have been flying off the shelves. But, don’t worry,\n                        you can start hunting for specific Moments on the marketplace\n                        now!\n                    "
              ),
            ]),
          ]),
          _vm._v(" "),
          _c("ul", { staticClass: "packs-list previous-list" }, [
            _c("li", { staticClass: "pack-item" }, [
              _c("div", { staticClass: "img-pack" }, [
                _c("img", {
                  attrs: {
                    src: "https://beatoken.com/wp-content/themes/beatoken/core/img/drop_platinum_pack.png",
                    width: "197",
                    height: "192",
                    alt: "pack-img",
                  },
                }),
              ]),
              _vm._v(" "),
              _c("h3", { staticClass: "pack-title" }, [
                _vm._v("B.O.C X COZY RUGZ"),
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "pack-set" }, [
                _vm._v("DROP 1 PLATINUM PACK"),
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "pack-price" }, [
                _vm._v("350 "),
                _c("span", [_vm._v("kr.")]),
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "pack-quantity sold" }, [
                _vm._v("58 "),
                _c("span", [_vm._v("sold out")]),
              ]),
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "pack-item" }, [
              _c("div", { staticClass: "img-pack" }, [
                _c("img", {
                  attrs: {
                    src: "https://beatoken.com/wp-content/themes/beatoken/core/img/drop_holo_pack.png",
                    width: "197",
                    height: "192",
                    alt: "pack-img",
                  },
                }),
              ]),
              _vm._v(" "),
              _c("h3", { staticClass: "pack-title" }, [
                _vm._v("BETA TEST DROP"),
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "pack-set" }, [_vm._v("Drop 0")]),
              _vm._v(" "),
              _c("p", { staticClass: "pack-price" }, [
                _vm._v("250 "),
                _c("span", [_vm._v("kr.")]),
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "pack-quantity sold" }, [
                _vm._v("10 "),
                _c("span", [_vm._v("sold out")]),
              ]),
            ]),
          ]),
        ]),
      ]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);