"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_DropBuy_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _onflow_fcl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @onflow/fcl */ "./node_modules/@onflow/fcl/dist/fcl.module.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'drop-buy',
  data: function data() {
    return {
      drop: {},
      settings: {},
      canBuy: false,
      timeToBuy: 0,
      timer: null,
      displayTime: ''
    };
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    this.$loader(true);
    this.$store.dispatch('drop_lines/myTimeToBuyDrop', this.$route.params.drop_id).then(function (resp) {
      if (resp.redirectToLine) {
        _this.$notify({
          title: 'The time that you had for payment has expired, you can line up again',
          type: 'error'
        });

        _this.$router.push('/drop-line/' + _this.$route.params.drop_id);
      }

      if (resp.redirectToDrop) _this.$router.push('/drop/' + _this.$route.params.drop_id);
      _this.dropLine = resp.dropLine;
      _this.timeToBuy = resp.timeToBuy;

      _this.startTimer();

      if (!_this.$route.query.session_id) setTimeout(function () {
        _this.$loader(false);
      }, 1000);
    });
  },
  created: function created() {
    var _this2 = this;

    this.getAllSettings();
    this.getDrop();
    _onflow_fcl__WEBPACK_IMPORTED_MODULE_0__.currentUser().subscribe(function (flowUser) {
      if (flowUser.loggedIn) {
        _this2.$store.commit('flow/setFlowUser', flowUser);
      } else {
        _this2.$notify({
          title: 'You must be logged into your blocto account',
          type: 'error'
        });

        _onflow_fcl__WEBPACK_IMPORTED_MODULE_0__.authenticate();
      }
    });
  },
  watch: {
    timeToBuy: function timeToBuy(time) {
      if (time === 0) {
        this.stopTimer();
      }
    }
  },
  computed: {
    flowUser: function flowUser() {
      return this.$store.getters['flow/getFlowUser'];
    },
    user: function user() {
      return this.$store.getters['users/getCurrentUser'];
    }
  },
  methods: {
    startTimer: function startTimer() {
      var _this3 = this;

      this.canBuy = true;
      this.timer = setInterval(function () {
        _this3.timeToBuy--;
        var minutes = parseInt(_this3.timeToBuy / 60, 10);
        var seconds = parseInt(_this3.timeToBuy % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        _this3.displayTime = "".concat(minutes, "min  ").concat(seconds, "sec");
      }, 1000);
    },
    stopTimer: function stopTimer() {
      clearTimeout(this.timer);
      this.canBuy = false;
      this.$store.dispatch('drop_lines/deleteByDrop', this.$route.params.drop_id);
      this.$router.push('/drops');
    },
    getAllSettings: function getAllSettings() {
      var _this4 = this;

      this.$store.dispatch('settings/allSettings').then(function (settings) {
        _this4.settings = settings;
      });
    },
    getDrop: function getDrop() {
      var _this5 = this;

      this.$store.dispatch('drops/showDrop', this.$route.params.drop_id).then(function (drop) {
        _this5.drop = drop;
        if (!drop.nfts.length) _this5.$router.push('/drop/' + _this5.$route.params.drop_id);
        if (_this5.$route.query.session_id) _this5.updatePaymentRefillBalance();
      })["catch"](function () {
        return _this5.$router.push('/drops');
      });
    },
    updatePaymentRefillBalance: function updatePaymentRefillBalance() {
      var _this6 = this;

      this.$loader(true);
      this.$store.dispatch('payments/updatePaymentRefillBalance', {
        session_id: this.$route.query.session_id
      }).then(function (resp) {
        console.log('resp', resp);

        _this6.$notify({
          text: 'Congratulations, your balance has been replenished',
          type: 'success'
        });

        _this6.buyDrop(_this6.drop);
      })["catch"](function (e) {
        console.log('e', e);

        _this6.$notify({
          text: 'The session has already expired',
          type: 'warn'
        });

        _this6.$loader(false);

        _this6.$router.push('/');
      });
    },
    buyByCard: function buyByCard(drop) {
      var _this7 = this;

      var commissionForBuyDrop = drop.price / 100 * (Number(this.settings.percent_drop_seller) + Number(this.settings.percent_drop_buyer)); // consider the commission

      var totalSum = commissionForBuyDrop + drop.nfts[0].price * drop.number_nfts;
      this.$store.dispatch('payments/throughStripeGateway', {
        buyAfterPaymentThisUrl: window.location.href,
        amount: Number(parseFloat(totalSum)).toFixed(8)
      }).then(function (resp) {
        window.location.href = resp.session.url;
      })["catch"](function (err) {
        _this7.$notify({
          title: 'Something went wrong, please try again later',
          type: 'error'
        });

        console.log('err', err);
      });
    },
    buyDrop: function buyDrop(drop) {
      var _this8 = this;

      if (!this.canBuy || !this.flowUser) {
        return false;
      }

      this.$store.dispatch('drops/showDrop', drop.id).then(function (drop) {
        if (!drop.nfts.length) {
          _this8.$notify({
            title: 'All tokens in this drop have been sold',
            type: 'warn'
          });

          _this8.$router.push('/drop/' + drop.id);

          return false;
        } // check if there are still tokens in the drop


        _this8.$store.dispatch('users/getMiddlemanUser').then(function (user) {
          if (_this8.settings.percent_drop_seller && _this8.settings.percent_drop_buyer && user.current_flow_account) {
            _this8.$loader(true);

            _this8.$store.dispatch('flow/checkBalance', _this8.flowUser.addr) // get user balance
            .then(function (res) {
              var balanceUser = res.encodedData.value;
              var commissionForBuyDrop = drop.price / 100 * (Number(_this8.settings.percent_drop_seller) + Number(_this8.settings.percent_drop_buyer)); // consider the commission

              var ids = [];
              var flowIds = [];

              while (drop.number_nfts > flowIds.length) {
                var nft = Object.values(drop.nfts)[Math.floor(Math.random() * drop.nfts.length)]; // Get random nft

                if (!flowIds.find(function (x) {
                  return x == nft.flow_id;
                })) {
                  ids.push(nft.id);
                  flowIds.push(nft.flow_id);
                } // Check if there are duplicate id's and push

              }

              var totalSum = commissionForBuyDrop + nft.price * ids.length;

              if (balanceUser < totalSum) {
                _this8.$notify({
                  title: 'You do not have enough funds on your balance to buy this Drop. You can top up the balance in your profile',
                  type: 'error'
                });

                _this8.$loader(false);

                return false;
              }

              _this8.$store.dispatch('flow/buyTokens', {
                middleman: user.current_flow_account.address,
                commission: commissionForBuyDrop,
                ownerNft: nft.flow_account.address,
                ids: flowIds,
                price: nft.price
              }).then(function (tx) {
                _this8.$notify({
                  title: 'The nft purchase request has been accepted. Processed within a minute'
                });

                _onflow_fcl__WEBPACK_IMPORTED_MODULE_0__.tx(tx).subscribe(function (res) {
                  if (res.status >= 4 && res.errorMessage == '') {
                    _this8.$store.dispatch('drops/buyDrop', {
                      id: drop.id,
                      ids: ids,
                      hash: tx.transactionId,
                      commission: commissionForBuyDrop,
                      price: nft.price
                    }).then(function () {
                      _this8.$notify({
                        title: 'Your collection has been updated',
                        type: 'success'
                      });

                      _this8.canBuy = false;

                      _this8.$loader(false);

                      _this8.$router.push('/my-collection');
                    });
                  } else if (res.status >= 4) {
                    console.log('error@res', res);

                    _this8.$notify({
                      title: 'Something went wrong. Please try again later',
                      type: 'error'
                    });

                    _this8.$loader(false);
                  }
                });
              })["catch"](function (e) {
                _this8.$loader(false);

                console.log('e', e);
              });
            })["catch"](function (e) {
              console.log(e);

              _this8.$notify({
                title: 'Error with your balance. Go to your profile for more information',
                type: 'error'
              });

              _this8.$loader(false);
            });
          } else {
            _this8.$notify({
              title: 'There must be a commission. Please try again later',
              type: 'error'
            });
          }
        });
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/DropBuy.vue":
/*!***********************************************!*\
  !*** ./resources/assets/js/pages/DropBuy.vue ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DropBuy.vue?vue&type=template&id=3c9ac22c& */ "./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c&");
/* harmony import */ var _DropBuy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DropBuy.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DropBuy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__.render,
  _DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/DropBuy.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropBuy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DropBuy.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DropBuy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c&":
/*!******************************************************************************!*\
  !*** ./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c& ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DropBuy_vue_vue_type_template_id_3c9ac22c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DropBuy.vue?vue&type=template&id=3c9ac22c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/DropBuy.vue?vue&type=template&id=3c9ac22c& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("main", { staticClass: "drop-full__screen-wrapper count" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "drop-count__container" }, [
          _c("div", { staticClass: "drop-count-wrapper" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "drop-count__title-block" }, [
              _c("h3", [_vm._v(_vm._s(_vm.drop.name))]),
              _vm._v(" "),
              _vm.drop.nfts
                ? _c("p", { staticClass: "pack-sale" }, [
                    _c("span", [_vm._v(_vm._s(_vm.drop.nfts.length))]),
                    _vm._v("total packs for sale"),
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("p", { staticClass: "pack-desc" }, [
                _vm._v(
                  "\n                            " +
                    _vm._s(_vm.drop.short_description) +
                    "\n                        "
                ),
              ]),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "drop-count__count-block" }, [
              _c("img", { attrs: { src: _vm.drop.full_uri, alt: "drop-img" } }),
              _vm._v(" "),
              _c("div", { staticClass: "event-timer__block" }, [
                _c("div", { staticClass: "timer" }, [
                  _vm._v(
                    "\n                                " +
                      _vm._s(_vm.displayTime) +
                      "\n                            "
                  ),
                ]),
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-12 flex flex-around" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    staticStyle: { color: "#fff" },
                    attrs: { disabled: !_vm.canBuy },
                    on: {
                      click: function ($event) {
                        return _vm.buyDrop(_vm.drop)
                      },
                    },
                  },
                  [_vm._v("Buy drop")]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    staticStyle: { color: "#fff" },
                    attrs: { disabled: !_vm.canBuy },
                    on: {
                      click: function ($event) {
                        return _vm.buyByCard(_vm.drop)
                      },
                    },
                  },
                  [_vm._v("Buy drop with card")]
                ),
              ]),
            ]),
          ]),
        ]),
      ]),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "section-border" }),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "drop-count__logo-block" }, [
      _c("img", {
        staticClass: "img-logo",
        attrs: { src: "assets/img/logo.png", alt: "logo" },
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "text-logo",
        attrs: { src: "assets/img/beatokenTextLogo.svg", alt: "text-logo" },
      }),
    ])
  },
]
render._withStripped = true



/***/ })

}]);