"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_assets_js_pages_admin_Settings_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'Settings',
  data: function data() {
    return {
      form: {
        percent_drop_seller: '',
        percent_drop_buyer: '',
        percent_one_token_seller: '',
        percent_one_token_buyer: '',
        percent_author_receive: '',
        currency_rate_dkk: '',
        default_time_drop_line: '',
        default_time_drop_payment: '',
        keys_number: ''
      }
    };
  },
  created: function created() {
    this.allSettings();
  },
  methods: {
    allSettings: function allSettings() {
      var _this = this;

      this.$store.dispatch('settings/allSettings').then(function (settings) {
        _this.form = settings;
      });
    },
    saveSettings: function saveSettings() {
      var _this2 = this;

      this.$store.dispatch('settings/addSettings', this.form).then(function (settings) {
        _this2.form = settings;
      });
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/pages/admin/Settings.vue":
/*!******************************************************!*\
  !*** ./resources/assets/js/pages/admin/Settings.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Settings.vue?vue&type=template&id=6cf90e40& */ "./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40&");
/* harmony import */ var _Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Settings.vue?vue&type=script&lang=js& */ "./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__.render,
  _Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/pages/admin/Settings.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Settings.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40&":
/*!*************************************************************************************!*\
  !*** ./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40& ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_6cf90e40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Settings.vue?vue&type=template&id=6cf90e40& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/pages/admin/Settings.vue?vue&type=template&id=6cf90e40& ***!
  \****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h3", { staticClass: "admin-table__title" }, [_vm._v("Nft settings")]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-bordered" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("tbody", [
        _c("tr", [
          _c("td", [
            _vm._v("Percentage of commission when selling through a drop"),
          ]),
          _vm._v(" "),
          _c("td", { staticClass: "flex-center" }, [
            _c("div", [
              _vm._v("Seller "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.percent_drop_seller,
                    expression: "form.percent_drop_seller",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.percent_drop_seller },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "percent_drop_seller",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", [
              _vm._v("Buyer "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.percent_drop_buyer,
                    expression: "form.percent_drop_buyer",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.percent_drop_buyer },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "percent_drop_buyer",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
          ]),
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [
            _vm._v("Percentage of commission for the sale of one token"),
          ]),
          _vm._v(" "),
          _c("td", { staticClass: "flex-center" }, [
            _c("div", [
              _vm._v("Seller "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.percent_one_token_seller,
                    expression: "form.percent_one_token_seller",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.percent_one_token_seller },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "percent_one_token_seller",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", [
              _vm._v("Buyer "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.percent_one_token_buyer,
                    expression: "form.percent_one_token_buyer",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.percent_one_token_buyer },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "percent_one_token_buyer",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
          ]),
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [
            _vm._v(
              "What percentage the author will receive. Remaining commission go to middleman"
            ),
          ]),
          _vm._v(" "),
          _c("td", { staticClass: "flex-center" }, [
            _c("div", [
              _vm._v("Author "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.percent_author_receive,
                    expression: "form.percent_author_receive",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.percent_author_receive },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "percent_author_receive",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
          ]),
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [_vm._v("DKK currency rate")]),
          _vm._v(" "),
          _c("td", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.currency_rate_dkk,
                  expression: "form.currency_rate_dkk",
                },
              ],
              staticClass: "form-control",
              attrs: { min: "0", type: "number" },
              domProps: { value: _vm.form.currency_rate_dkk },
              on: {
                input: function ($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "currency_rate_dkk", $event.target.value)
                },
              },
            }),
          ]),
        ]),
        _vm._v(" "),
        _c("tr", [
          _c("td", [
            _vm._v(
              "The waiting time for the line and the time that is given for the purchase of a token"
            ),
          ]),
          _vm._v(" "),
          _c("td", { staticClass: "flex-center" }, [
            _c("div", [
              _vm._v("Time line (sec) "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.default_time_drop_line,
                    expression: "form.default_time_drop_line",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.default_time_drop_line },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "default_time_drop_line",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
            _vm._v(" "),
            _c("div", [
              _vm._v("Buy drop (sec) "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.default_time_drop_payment,
                    expression: "form.default_time_drop_payment",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.default_time_drop_payment },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "default_time_drop_payment",
                      $event.target.value
                    )
                  },
                },
              }),
            ]),
          ]),
        ]),
        _vm._v(" "),
        _c("tr", [
          _vm._m(1),
          _vm._v(" "),
          _c("td", { staticClass: "flex-center" }, [
            _c("div", [
              _vm._v("Keys number "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.keys_number,
                    expression: "form.keys_number",
                  },
                ],
                staticClass: "form-control",
                attrs: { min: "0", type: "number" },
                domProps: { value: _vm.form.keys_number },
                on: {
                  input: function ($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "keys_number", $event.target.value)
                  },
                },
              }),
            ]),
          ]),
        ]),
      ]),
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "flex flex-center" }, [
      _c(
        "button",
        { staticClass: "btn btn-success", on: { click: _vm.saveSettings } },
        [_c("h3", [_vm._v("Save")])]
      ),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Description")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Value")]),
      ]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _vm._v(
        "The number of keys used to sign the FLOW transactions.\n                    "
      ),
      _c("span", { staticStyle: { color: "#f86767" } }, [
        _vm._v(
          "DO NOT CHANGE THE VALUE UNLESS THE cadency/flow.json FILE HAS BEEN UPDATED"
        ),
      ]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);